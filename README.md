
## 帮助文档

表达式引擎
```
[
	[
		["{order.start_time}", ">=", "CURRENT_TIME"],
		["{order.end_time}", "<=", "CURRENT_TIME"],
		["{rights.verify_status}", "==", "1"],
		["{rights.cancel_status}", "==", "1"]
	],
	[
		["{order.start_time}", ">=", "CURRENT_TIME"],
		["{order.end_time}", "<=", "CURRENT_TIME"],
		["{rights.verify_status}", "==", "1"],
		["{rights.cancel_status}", "==", "1"],
		["{rights.child_rights.[sku_item_id={intput.sku_item_id}].max_count} - {rights.child_rights.[sku_item_id={intput.sku_item_id}].use_count}]", "<=", "{input.count}"]
	]
]

```
