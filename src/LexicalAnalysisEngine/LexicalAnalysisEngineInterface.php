<?php
declare(strict_types=1);

namespace ExpressionEngine\LexicalAnalysisEngine;

interface LexicalAnalysisEngineInterface
{
    /**
     * @param string $expression
     * @return array
     */
    public function scanning(string $expression): array;
}