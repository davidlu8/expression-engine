<?php
declare(strict_types=1);

namespace ExpressionEngine\LexicalAnalysisEngine\Enum;

class LexicalAnalysisEnum
{
    const SCAN_KEY_EXPRESSION_NAME = 'expression';

    const SCAN_KEY_LOOP_LIST_NAME = 'loop_list';
    const SCAN_KEY_EXPRESSION_LIST_NAME = 'expression_list';
    const SCAN_KEY_VARIABLE_LIST_NAME = 'variable_list';
    const SCAN_KEY_CONSTANT_LIST_NAME = 'constant_list';

    const TAG_ID_LOOP = 'L';
    const TAG_ID_EXPRESSION = 'E';
    const TAG_ID_VARIABLE = 'V';
    const TAG_ID_CONSTANT = 'C';
}