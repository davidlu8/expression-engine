<?php
declare(strict_types=1);

namespace ExpressionEngine\LexicalAnalysisEngine\Loop;

interface LexicalAnalysisLoopInterface
{
    public function is(string $expression): bool;

    public function validate(string $expression): void;

    public function scan(string &$expression, array $loopList): array;
}