<?php
declare(strict_types=1);

namespace ExpressionEngine\Utils;

class StringUtil
{
    public static function formatExpression(string $expression): string
    {
        $expression = trim($expression);
        return static::formatBlank($expression);
    }

    public static function formatBlank(string $value): string
    {
        return preg_replace('/\s+/', ' ', $value);
    }

    public static function formatSymbol(string $symbol): string
    {
        return static::replaceBlank($symbol);
    }

    public static function replaceBlank(string $value): string
    {
        return preg_replace('/\s+/', '', $value);
    }

}