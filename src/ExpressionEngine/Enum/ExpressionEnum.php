<?php
declare(strict_types=1);

namespace ExpressionEngine\ExpressionEngine\Enum;

class ExpressionEnum
{
    const COMPILE_ID_LOOP_LIST = 'compile_loop_list';
    const COMPILE_ID_EXPRESSION_LIST = 'compile_expression_list';
    const COMPILE_ID_VARIABLE_LIST = 'compile_variable_list';
    const COMPILE_ID_CONSTANT_LIST = 'compile_constant_list';

}