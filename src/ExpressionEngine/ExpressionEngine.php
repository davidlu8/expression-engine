<?php
declare(strict_types=1);

namespace ExpressionEngine\ExpressionEngine;

use Contract\Exceptions\LogicException;
use ExpressionEngine\ExpressionEngine\Enum\ExpressionEnum;

class ExpressionEngine implements ExpressionEngineInterface
{
    protected ExpressionCompile $expressionCompile;

    public function __construct(ExpressionCompile $expressionCompile)
    {
        $this->expressionCompile = $expressionCompile;
    }

    /**
     * @param string $expression
     * @param array $compileContext
     * @return mixed
     * @throws LogicException
     */
    public function compile(string $expression, array $compileContext): mixed
    {
        return $this->expressionCompile->exec($expression, $compileContext);
    }

    /**
     * @param array $expressionList
     * @param array $compileContext
     * @return mixed
     * @throws LogicException
     */
    public function compileList(array $expressionList, array $compileContext): array
    {
        $compileExpressionList = [];
        foreach ($expressionList as $key => $expression) {
            $compileExpressionList[$key] = $this->compile($expression, $compileContext);
            $compileContext[ExpressionEnum::COMPILE_ID_EXPRESSION_LIST] = $compileExpressionList;
        }
        return $compileExpressionList;
    }

}