<?php
declare(strict_types=1);

namespace ExpressionEngine\ExpressionEngine;

interface ExpressionEngineInterface
{
    /**
     * @param string $expression
     * @param array $compileContext
     * @return mixed
     */
    public function compile(string $expression, array $compileContext): mixed;


    /**
     * @param array $expressionList
     * @param array $compileContext
     * @return array
     */
    public function compileList(array $expressionList, array $compileContext): array;

}