<?php
declare(strict_types=1);

namespace ExpressionEngine\OperatorEngine\SpecialOperator;

use Contract\Exceptions\LogicException;
use ExpressionEngine\OperatorEngine\Enum\OperatorCompareEnum;

class SpecialOperatorIn implements SpecialOperatorInterface
{
    /**
     * @param array $symbols
     * @return array
     * @throws LogicException
     */
    public function parse(array $symbols): array
    {
        $count = 0;
        foreach ($symbols as $symbol) {
            if ($symbol == OperatorCompareEnum::IN) {
                $count++;
            }
        }
        if ($count != 1) {
            throw new LogicException('特殊符号' . OperatorCompareEnum::IN . '出现的次数不正确 次数：' . $count);
        }

        $index = array_search(OperatorCompareEnum::IN, $symbols);
        return [
            sprintf('in_array(%s, %s)', implode(' ', array_slice($symbols, 0, $index)), implode(' ', array_slice($symbols, $index + 1))),
        ];
    }
}