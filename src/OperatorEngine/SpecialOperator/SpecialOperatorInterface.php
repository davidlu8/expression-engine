<?php
declare(strict_types=1);

namespace ExpressionEngine\OperatorEngine\SpecialOperator;

interface SpecialOperatorInterface
{
    public function parse(array $symbols): array;

}