<?php
declare(strict_types=1);

namespace ExpressionEngine\OperatorEngine\Enum;

use ExpressionEngine\OperatorEngine\SpecialOperator\SpecialOperatorIn;

class OperatorCompareEnum
{
    public const COMMON_LIST = [
        self::EQUAL_TO,
        self::NOT_EQUAL_TO,
        self::LESS_THAN,
        self::LESS_THAN_OR_EQUAL_TO,
        self::GREATER_THAN,
        self::GREATER_THAN_OR_EQUAL_TO,
    ];

    public const SPECIAL_LIST = [
        self::IN => SpecialOperatorIn::class
    ];

    public const EQUAL_TO = '==';
    public const NOT_EQUAL_TO = '!=';
    public const LESS_THAN = '<';
    public const LESS_THAN_OR_EQUAL_TO = '<=';
    public const GREATER_THAN = '<';
    public const GREATER_THAN_OR_EQUAL_TO = '<=';

    public const IN = 'in';
}