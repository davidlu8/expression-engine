<?php
declare(strict_types=1);

namespace ExpressionEngine\OperatorEngine\Enum;

class OperatorMathematicsEnum
{
    public const LIST = [
        self::PLUS,
        self::REDUCE,
        self::MULTIPLICATION,
        self::DIVISION
    ];

    public const PLUS = '+';
    public const REDUCE = '-';
    public const MULTIPLICATION = '*';
    public const DIVISION = '/';
}