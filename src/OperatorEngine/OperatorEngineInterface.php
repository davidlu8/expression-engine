<?php
declare(strict_types=1);

namespace ExpressionEngine\OperatorEngine;

interface OperatorEngineInterface
{
    /**
     * @param string $operator
     * @return bool
     */
    public function is(string $operator): bool;

    /**
     * @param string $constant
     * @return mixed
     */
    public function compile(string $constant): mixed;
}