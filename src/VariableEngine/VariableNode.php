<?php
declare(strict_types=1);

namespace ExpressionEngine\VariableEngine;

class VariableNode
{
    public function getNode(string $code, array $values): array
    {
        return array_merge([$code], $values);
    }
}