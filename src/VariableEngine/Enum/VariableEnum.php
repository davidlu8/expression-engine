<?php
declare(strict_types=1);

namespace ExpressionEngine\VariableEngine\Enum;

class VariableEnum
{
    const NODE_KEY = 'K';
    const NODE_SEARCH = 'S';

}