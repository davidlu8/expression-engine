<?php
declare(strict_types=1);

namespace ExpressionEngine\VariableEngine;

interface VariableEngineInterface
{
    /**
     * @param string $variable
     * @return bool
     */
    public function is(string $variable): bool;

    /**
     * @param string $variable
     * @param array $context
     * @param array $variableCompileList
     * @return mixed
     */
    public function compile(string $variable, array $context, array $variableCompileList): mixed;

    /**
     * @param array $variableList
     * @param array $context
     * @return array
     */
    public function compileList(array $variableList, array $context): array;
}