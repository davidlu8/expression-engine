<?php
declare(strict_types=1);

namespace ExpressionEngine\LoopEngine\Loop;

interface LoopInterface
{
    public function is(array $loopItem): bool;

    public function compile(array $loopItem, array $context): mixed;
}