<?php
declare(strict_types=1);

namespace ExpressionEngine\LoopEngine;

interface LoopEngineInterface
{
    /**
     * @param array $loopItem
     * @param array $context
     * @return mixed
     */
    public function compile(array $loopItem, array $context): mixed;

    /**
     * @param array $loopList
     * @param array $context
     * @return mixed
     */
    public function compileList(array $loopList, array $context, array $compileContext): mixed;
}