<?php
declare(strict_types=1);

namespace ExpressionEngine\LoopEngine\Enum;

use ExpressionEngine\LexicalAnalysisEngine\Loop\LexicalAnalysisLoopForeach;
use ExpressionEngine\LoopEngine\Loop\LoopForeach;

class LoopEnum
{
    const LEXICAL_ANALYSIS_LOOP_LIST = [
        self::LOOP_FOREACH_NAME => LexicalAnalysisLoopForeach::class
    ];

    const COMPILE_LOOP_LIST = [
        self::LOOP_FOREACH_NAME => LoopForeach::class
    ];

    const LOOP_FOREACH_NAME = 'foreach';
    const LOOP_FOREACH_AS = 'as';
    const LOOP_FOREACH_ARROW = '=>';
    const LOOP_FOREACH_HEADER_NAME = 'header';
    const LOOP_FOREACH_HEADER_TYPE_NAME = 'type';
    const LOOP_FOREACH_HEADER_LIST_NAME = 'list';
    const LOOP_FOREACH_HEADER_KEY_NAME = 'key';
    const LOOP_FOREACH_HEADER_VALUE_NAME = 'value';
}