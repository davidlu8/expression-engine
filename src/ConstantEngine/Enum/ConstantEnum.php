<?php
declare(strict_types=1);

namespace ExpressionEngine\ConstantEngine\Enum;

class ConstantEnum
{
    public const LIST = [
        self::CURRENT_DATE_TIME,
        self::CURRENT_DATE,
        self::CURRENT_TIME,
        self::CURRENT_YEAR,
        self::CURRENT_MONTH,
        self::CURRENT_DAY,
        self::CURRENT_HOUR,
        self::CURRENT_MINUTE,
        self::CURRENT_SECOND,
    ];

    /** @var string 当前日期和时间 2022-01-01 08:00:00 */
    public const CURRENT_DATE_TIME = 'CURRENT_DATE_TIME';
    /** @var string 当前日期 2022-01-01 */
    public const CURRENT_DATE = 'CURRENT_DATE';
    /** @var string 当前时间 08:00:00 */
    public const CURRENT_TIME = 'CURRENT_TIME';
    /** @var string 当前年份 2022 */
    public const CURRENT_YEAR = 'CURRENT_YEAR';
    /** @var string 当前月份 1 */
    public const CURRENT_MONTH = 'CURRENT_MONTH';
    /** @var string 当前日 1 */
    public const CURRENT_DAY = 'CURRENT_DAY';
    /** @var string 当前小时 8 */
    public const CURRENT_HOUR = 'CURRENT_HOUR';
    /** @var string 当前分钟 0 */
    public const CURRENT_MINUTE = 'CURRENT_MINUTE';
    /** @var string 当前秒 0 */
    public const CURRENT_SECOND = 'CURRENT_SECOND';

}