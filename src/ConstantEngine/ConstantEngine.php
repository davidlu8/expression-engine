<?php
declare(strict_types=1);

namespace ExpressionEngine\ConstantEngine;

use Contract\Exceptions\LogicException;
use Contract\Exceptions\ValidationException;
use ExpressionEngine\ConstantEngine\Enum\ConstantEnum;

class ConstantEngine implements ConstantEngineInterface
{
    public function is(string $constant): bool
    {
        return in_array($constant, ConstantEnum::LIST);
    }

    /**
     * @param string $constant
     * @return mixed
     * @throws LogicException
     * @throws ValidationException
     */
    public function compile(string $constant): mixed
    {
        if (!$this->is($constant)) {
            throw new ValidationException('the constant is not correct');
        }
        return match ($constant) {
            ConstantEnum::CURRENT_DATE_TIME => date('Y-m-d H:i:s'),
            ConstantEnum::CURRENT_DATE => date('Y-m-d'),
            ConstantEnum::CURRENT_TIME => date('H:i:s'),
            ConstantEnum::CURRENT_YEAR => date('Y'),
            ConstantEnum::CURRENT_MONTH => date('m'),
            ConstantEnum::CURRENT_DAY => date('d'),
            ConstantEnum::CURRENT_HOUR => date('H'),
            ConstantEnum::CURRENT_MINUTE => date('i'),
            ConstantEnum::CURRENT_SECOND => date('s'),
            default => throw new LogicException('constant:' . $constant . ' is not valid'),
        };
    }

    /**
     * @param array $constantList
     * @return array
     * @throws LogicException
     * @throws ValidationException
     */
    public function compileList(array $constantList): array
    {
        $compileConstantList = [];
        foreach ($constantList as $key => $constant) {
            $compileConstantList[$key] = $this->compile($constant);
        }
        return $compileConstantList;
    }

}