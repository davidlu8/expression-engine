<?php
declare(strict_types=1);

namespace ExpressionEngine\ConstantEngine;

interface ConstantEngineInterface
{
    /**
     * @param string $constant
     * @return bool
     */
    public function is(string $constant): bool;

    /**
     * @param string $constant
     * @return mixed
     */
    public function compile(string $constant): mixed;


    /**
     * @param array $constantList
     * @return array
     */
    public function compileList(array $constantList): array;

}