<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests\LexicalAnalysisEngine;

use Contract\Exceptions\LogicException;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisExpression;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisTag;
use ExpressionEngine\Tests\TestCase;

class LexicalAnalysisExpressionTest extends TestCase
{
    protected LexicalAnalysisExpression $lexicalAnalysisExpression;

    public function setUp(): void
    {
        parent::setUp();
        $this->lexicalAnalysisExpression = new LexicalAnalysisExpression(new LexicalAnalysisTag());
    }

    public function dpScan(): array
    {
        return [
            [
                '(1 + 2) / 3 + 5',
                [
                    '1 + 2',
                    '<E,0> / 3 + 5',
                ]
            ],
            [
                '((1 + 2) * 3) * (5 + 2)',
                [
                    '1 + 2',
                    '<E,0> * 3',
                    '5 + 2',
                    '<E,1> * <E,2>'
                ],

            ]
        ];
    }

    /**
     * @dataProvider dpScan
     * @param string $expression
     * @param array $childExpressionList
     * @return void
     * @throws LogicException
     */
    public function testScan(string $expression, array $childExpressionList): void
    {
        $this->assertEquals($childExpressionList, $this->lexicalAnalysisExpression->scan($expression));
    }
}