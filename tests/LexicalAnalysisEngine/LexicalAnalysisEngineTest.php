<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests\LexicalAnalysisEngine;

use Contract\Exceptions\LogicException;
use Contract\Exceptions\ValidationException;
use ExpressionEngine\ConstantEngine\ConstantEngine;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisEngine;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisExpression;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisLoop;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisSymbol;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisTag;
use ExpressionEngine\Tests\ContainerMock;
use ExpressionEngine\Tests\TestCase;
use ExpressionEngine\VariableEngine\VariableCompile;
use ExpressionEngine\VariableEngine\VariableEngine;
use ExpressionEngine\VariableEngine\VariableLexical;
use ExpressionEngine\VariableEngine\VariableNode;

class LexicalAnalysisEngineTest extends TestCase
{
    protected LexicalAnalysisEngine $lexicalAnalysisEngine;

    public function setUp(): void
    {
        parent::setUp();
        $lexicalAnalysisLoop = new LexicalAnalysisLoop(new ContainerMock());
        $lexicalAnalysisExpression = new LexicalAnalysisExpression(new LexicalAnalysisTag());
        $lexicalAnalysisSymbol = new LexicalAnalysisSymbol(new LexicalAnalysisTag(), new VariableEngine(new VariableLexical(new VariableNode()), new VariableCompile(new LexicalAnalysisTag())), new ConstantEngine());
        $this->lexicalAnalysisEngine = new LexicalAnalysisEngine($lexicalAnalysisLoop, $lexicalAnalysisExpression, $lexicalAnalysisSymbol);

    }

    /**
     * @return array
     */
    public function dpScanning(): array
    {
        return [
            [
                '({order[status={input.status}].start_time} + 1) * 3 + CURRENT_DATE_TIME',
                [
                    'expression' => '({order[status={input.status}].start_time} + 1) * 3 + CURRENT_DATE_TIME',
                    'expression_list' => [
                        '<V,1> + 1',
                        '<E,0> * 3 + <C,0>'
                    ],
                    'variable_list' => [
                        'input.status',
                        'order[status=<V,0>].start_time'
                    ],
                    'constant_list' => [
                        'CURRENT_DATE_TIME'
                    ],
                    'loop_list' => []
                ]
            ],
            [
                'foreach({operation.child_rights} as {key} => {value}): {rights.child_rights.[original_id={value.original_id}].available_count} - {value.count} >= 0',
                [

                    'expression' => 'foreach({operation.child_rights} as {key} => {value}): {rights.child_rights.[original_id={value.original_id}].available_count} - {value.count} >= 0',
                    'loop_list' => [
                        [
                            'header' => [
                                'type' => 'foreach',
                                'list' => 'operation.child_rights',
                                'key' => 'key',
                                'value' => 'value'
                            ],
                            'expression' => '{rights.child_rights.[original_id={value.original_id}].available_count} - {value.count} >= 0',
                            'expression_list' => [
                                '<V,1> - <V,2> >= 0',
                            ],
                            'variable_list' => [
                                'value.original_id',
                                'rights.child_rights.[original_id=<V,0>].available_count',
                                'value.count',
                            ],
                            'constant_list' => [],
                            'loop_list' => []
                        ]
                    ],
                    'expression_list' => ['<L,0>'],
                    'variable_list' => [],
                    'constant_list' => []
                ]
            ]
        ];
    }

    /**
     * @dataProvider dpScanning
     * @param string $expression
     * @param array $data
     * @return void
     * @throws LogicException
     * @throws ValidationException
     */
    public function testScanning(string $expression, array $data): void
    {
        $a = $this->lexicalAnalysisEngine->scanning($expression);
        $this->assertEquals($data, $a);
    }
}