<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests\LexicalAnalysisEngine;

use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisLoop;
use ExpressionEngine\Tests\ContainerMock;
use ExpressionEngine\Tests\TestCase;

class LexicalAnalysisLoopTest extends TestCase
{
    protected LexicalAnalysisLoop $lexicalAnalysisExpression;

    public function setUp(): void
    {
        $this->lexicalAnalysisExpression = new LexicalAnalysisLoop(new ContainerMock());
    }


    public function dpScan(): array
    {
        return [
            [
                'foreach({items} as {key} => {value}): {value.id} >= 10',
                [
                    [
                        'header' => [
                            'type' => 'foreach',
                            'list' => 'items',
                            'key' => 'key',
                            'value' => 'value',
                        ],
                        'expression' => '{value.id} >= 10'
                    ]
                ]
            ]
        ];

    }

    /**
     * @dataProvider dpScan
     * @param string $expression
     * @param array $expected
     * @return void
     */
    public function testScan(string $expression, array $expected): void
    {
        $this->assertEquals($expected, $this->lexicalAnalysisExpression->scan($expression));
    }
}