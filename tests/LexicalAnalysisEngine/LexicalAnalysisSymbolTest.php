<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests\LexicalAnalysisEngine;

use ExpressionEngine\ConstantEngine\ConstantEngine;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisSymbol;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisTag;
use ExpressionEngine\Tests\TestCase;
use ExpressionEngine\VariableEngine\VariableCompile;
use ExpressionEngine\VariableEngine\VariableEngine;
use ExpressionEngine\VariableEngine\VariableLexical;
use ExpressionEngine\VariableEngine\VariableNode;

class LexicalAnalysisSymbolTest extends TestCase
{
    protected LexicalAnalysisSymbol $lexicalAnalysisVariable;

    public function setUp(): void
    {
        parent::setUp();
        $this->lexicalAnalysisVariable = new LexicalAnalysisSymbol(new LexicalAnalysisTag(), new VariableEngine(new VariableLexical(new VariableNode()), new VariableCompile(new LexicalAnalysisTag())), new ConstantEngine());
    }

    public function dpScan(): array
    {
        return [
            [
                '{child.verify} + 1 * 3 + CURRENT_DATE_TIME',
                '<V,0> + 1 * 3 + <C,0>',
                [
                    'variable_list' => [
                        'child.verify'
                    ],
                    'constant_list' => [
                        'CURRENT_DATE_TIME'
                    ]
                ]
            ],
            [
                '{child.items.verify_status} + {child.item.cancel_status} + 3',
                '<V,0> + <V,1> + 3',
                [
                    'variable_list' => [
                        'child.items.verify_status',
                        'child.item.cancel_status'
                    ],
                    'constant_list' => []
                ]
            ],
            [
                '{child.items.[original_id={input.original_id}].verify_status} + {child.item.cancel_status} + 3',
                '<V,1> + <V,2> + 3',
                [
                    'variable_list' => [
                        'input.original_id',
                        'child.items.[original_id=<V,0>].verify_status',
                        'child.item.cancel_status'
                    ],
                    'constant_list' => []
                ]
            ]
        ];
    }

    /**
     * @dataProvider dpScan
     * @param string $originalExpression
     * @param string $replacedExpression
     * @param array $symbolList
     * @return void
     */
    public function testScan(string $originalExpression, string $replacedExpression, array $symbolList): void
    {
        $actualSymbolList = [];
        $this->assertEquals($replacedExpression, $this->lexicalAnalysisVariable->scan($originalExpression, $actualSymbolList));
        $this->assertEquals($symbolList, $actualSymbolList);
    }
}