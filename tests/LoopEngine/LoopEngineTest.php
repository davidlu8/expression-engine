<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests\LoopEngine;

use ExpressionEngine\ConstantEngine\ConstantEngine;
use ExpressionEngine\ExpressionEngine\ExpressionCompile;
use ExpressionEngine\ExpressionEngine\ExpressionEngine;
use ExpressionEngine\ExpressionLogic;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisEngine;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisExpression;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisLoop;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisSymbol;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisTag;
use ExpressionEngine\LoopEngine\LoopEngine;
use ExpressionEngine\Tests\ContainerMock;
use ExpressionEngine\Tests\TestCase;
use ExpressionEngine\VariableEngine\VariableCompile;
use ExpressionEngine\VariableEngine\VariableEngine;
use ExpressionEngine\VariableEngine\VariableLexical;
use ExpressionEngine\VariableEngine\VariableNode;

class LoopEngineTest extends TestCase
{
    protected LoopEngine $loopEngine;

    public function setUp(): void
    {
        $lexicalAnalysisTag = new LexicalAnalysisTag();
        $variableEngine = new VariableEngine(new VariableLexical(new VariableNode()), new VariableCompile($lexicalAnalysisTag));
        $constantEngine = new ConstantEngine();
        $lexicalAnalysisEngine = new LexicalAnalysisEngine(new LexicalAnalysisLoop(new ContainerMock()), new LexicalAnalysisExpression($lexicalAnalysisTag), new LexicalAnalysisSymbol($lexicalAnalysisTag, $variableEngine, $constantEngine));
        $expressionLogic = new ExpressionLogic($lexicalAnalysisEngine, $variableEngine, $constantEngine, new ExpressionEngine(new ExpressionCompile(new ContainerMock(), $lexicalAnalysisTag)));
        $this->loopEngine = new LoopEngine(new ContainerMock(), $expressionLogic);
    }

    public function dpCompile(): array
    {
        return [
            [
                [
                    'header' => [
                        'type' => 'foreach',
                        'list' => 'operation.child_rights',
                        'key' => 'key',
                        'value' => 'value'
                    ],
                    'expression' => '{rights.child_rights.[original_id={value.original_id}].available_count} - {value.count} >= 0',
                    'expression_list' => [
                        '<V,1> - <V,2> >= 0',
                    ],
                    'variable_list' => [
                        'value.original_id',
                        'rights.child_rights.[original_id=<V,0>].available_count',
                        'value.count',
                    ],
                    'constant_list' => [],
                    'loop_list' => []
                ],
                [
                    'rights' => [
                        'child_rights' => [
                            [
                                'original_id' => 1,
                                'available_count' => 5,
                            ],
                            [
                                'original_id' => 2,
                                'available_count' => 3,
                            ],
                            [
                                'original_id' => 3,
                                'available_count' => 1,
                            ],
                        ]
                    ],
                    'operation' => [
                        'child_rights' => [
                            [
                                'original_id' => 1,
                                'count' => 3
                            ],
                            [
                                'original_id' => 2,
                                'count' => 2
                            ]
                        ]
                    ]
                ],
                true
            ],
            [
                [
                    'header' => [
                        'type' => 'foreach',
                        'list' => 'operation.child_rights',
                        'key' => 'key',
                        'value' => 'value'
                    ],
                    'expression' => '{rights.child_rights.[original_id={value.original_id}].available_count} - {value.count} >= 0',
                    'expression_list' => [
                        '<V,1> - <V,2> >= 0',
                    ],
                    'variable_list' => [
                        'value.original_id',
                        'rights.child_rights.[original_id=<V,0>].available_count',
                        'value.count',
                    ],
                    'constant_list' => [],
                    'loop_list' => []
                ],
                [
                    'rights' => [
                        'child_rights' => [
                            [
                                'original_id' => 1,
                                'available_count' => 5,
                            ],
                            [
                                'original_id' => 2,
                                'available_count' => 3,
                            ],
                            [
                                'original_id' => 3,
                                'available_count' => 1,
                            ],
                        ]
                    ],
                    'operation' => [
                        'child_rights' => [
                            [
                                'original_id' => 2,
                                'count' => 3
                            ],
                            [
                                'original_id' => 3,
                                'count' => 2
                            ]
                        ]
                    ]
                ],
                false
            ]
        ];
    }

    /**
     * @dataProvider dpCompile
     * @param array $loopItem
     * @param array $context
     * @param bool $expected
     * @return void
     */
    public function testCompile(array $loopItem, array $context, bool $expected): void
    {
        $this->assertEquals($expected, $this->loopEngine->compile($loopItem, $context));
    }

    public function dpCompileList(): array
    {
        return [
            [
                [
                    [
                        'header' => [
                            'type' => 'foreach',
                            'list' => 'operation.child_rights',
                            'key' => 'key',
                            'value' => 'value'
                        ],
                        'expression' => '{rights.child_rights.[original_id={value.original_id}].available_count} - {value.count} >= 0',
                        'expression_list' => [
                            '<V,1> - <V,2> >= 0',
                        ],
                        'variable_list' => [
                            'value.original_id',
                            'rights.child_rights.[original_id=<V,0>].available_count',
                            'value.count',
                        ],
                        'constant_list' => [],
                        'loop_list' => []
                    ]
                ],
                [
                    'rights' => [
                        'child_rights' => [
                            [
                                'original_id' => 1,
                                'available_count' => 5,
                            ],
                            [
                                'original_id' => 2,
                                'available_count' => 3,
                            ],
                            [
                                'original_id' => 3,
                                'available_count' => 1,
                            ],
                        ]
                    ],
                    'operation' => [
                        'child_rights' => [
                            [
                                'original_id' => 1,
                                'count' => 3
                            ],
                            [
                                'original_id' => 2,
                                'count' => 2
                            ]
                        ]
                    ]
                ],
                [],
                [
                    true,
                ]
            ],
        ];
    }

    /**
     * @dataProvider dpCompileList
     * @param array $loopList
     * @param array $context
     * @param array $compileContext
     * @param array $expected
     * @return void
     */
    public function testCompileList(array $loopList, array $context, array $compileContext, array $expected): void
    {
        $this->assertEquals($expected, $this->loopEngine->compileList($loopList, $context, $compileContext));
    }

}