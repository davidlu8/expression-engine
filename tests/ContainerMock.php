<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests;

use ExpressionEngine\ConstantEngine\ConstantEngine;
use ExpressionEngine\ExpressionEngine\ExpressionCompile;
use ExpressionEngine\ExpressionEngine\ExpressionEngine;
use ExpressionEngine\ExpressionLogic;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisEngine;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisExpression;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisLoop;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisSymbol;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisTag;
use ExpressionEngine\LexicalAnalysisEngine\Loop\LexicalAnalysisLoopForeach;
use ExpressionEngine\LoopEngine\Loop\LoopForeach;
use ExpressionEngine\OperatorEngine\SpecialOperator\SpecialOperatorIn;
use ExpressionEngine\VariableEngine\VariableCompile;
use ExpressionEngine\VariableEngine\VariableEngine;
use ExpressionEngine\VariableEngine\VariableLexical;
use ExpressionEngine\VariableEngine\VariableNode;
use Psr\Container\ContainerInterface;

class ContainerMock implements ContainerInterface
{
    public function get(string $id): mixed
    {
        if ($id == LexicalAnalysisLoopForeach::class) {
            $lexicalAnalysisTag = new LexicalAnalysisTag();
            $variableEngine = new VariableEngine(new VariableLexical(new VariableNode()), new VariableCompile($lexicalAnalysisTag));
            return new LexicalAnalysisLoopForeach($variableEngine, $lexicalAnalysisTag);
        }
        if ($id == LoopForeach::class) {
            $lexicalAnalysisTag = new LexicalAnalysisTag();
            $variableEngine = new VariableEngine(new VariableLexical(new VariableNode()), new VariableCompile($lexicalAnalysisTag));
            $constantEngine = new ConstantEngine();
            $lexicalAnalysisEngine = new LexicalAnalysisEngine(new LexicalAnalysisLoop(new ContainerMock()), new LexicalAnalysisExpression($lexicalAnalysisTag), new LexicalAnalysisSymbol($lexicalAnalysisTag, $variableEngine, $constantEngine));
            $expressionLogic = new ExpressionLogic($lexicalAnalysisEngine, $variableEngine, $constantEngine, new ExpressionEngine(new ExpressionCompile(new ContainerMock(), $lexicalAnalysisTag)));
            return new LoopForeach($expressionLogic, $variableEngine, $lexicalAnalysisTag);
        }
        if ($id == SpecialOperatorIn::class) {
            return new SpecialOperatorIn();
        }
        return null;
    }

    public function has(string $id): bool
    {
        return false;
    }

}