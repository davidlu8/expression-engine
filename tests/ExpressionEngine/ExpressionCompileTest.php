<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests\ExpressionEngine;

use Contract\Exceptions\LogicException;
use ExpressionEngine\ExpressionEngine\ExpressionCompile;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisTag;
use ExpressionEngine\Tests\ContainerMock;
use ExpressionEngine\Tests\TestCase;

class ExpressionCompileTest extends TestCase
{
    protected ExpressionCompile $expressionCompile;

    public function setUp(): void
    {
        parent::setUp();
        $this->expressionCompile = new ExpressionCompile(new ContainerMock(), new LexicalAnalysisTag());
    }

    public function dpExec(): array
    {
        return [
            [
                '<E,0> + 1 + <V,0> + <C,0>',
                [
                    'compile_expression_list' => [
                        '1'
                    ],
                    'compile_variable_list' => [
                        2
                    ],
                    'compile_constant_list' => [
                        3
                    ]
                ],
                7
            ],
            [
                '<E,0> + 1 + <V,0> > <C,0>',
                [
                    'compile_expression_list' => [
                        '1'
                    ],
                    'compile_variable_list' => [
                        2
                    ],
                    'compile_constant_list' => [
                        3
                    ]
                ],
                true
            ],
            [
                '<E,0> + 1 + <C,0> in <V,0>',
                [
                    'compile_expression_list' => [
                        '1'
                    ],
                    'compile_variable_list' => [
                        '[4, 5]'
                    ],
                    'compile_constant_list' => [
                        2
                    ]
                ],
                true
            ],
            [
                '<E,0> + 1 + <C,0> in <V,0>',
                [
                    'compile_expression_list' => [
                        '1'
                    ],
                    'compile_variable_list' => [
                        '[2, 5]'
                    ],
                    'compile_constant_list' => [
                        2
                    ]
                ],
                false
            ]
        ];
    }

    /**
     * @dataProvider dpExec
     * @param $expression
     * @param $compileContext
     * @param $value
     * @return void
     * @throws LogicException
     */
    public function testExec($expression, $compileContext, $value): void
    {
        $this->assertEquals($value, $this->expressionCompile->exec($expression, $compileContext));
    }

}