<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests\ExpressionEngine;

use Contract\Exceptions\LogicException;
use ExpressionEngine\ExpressionEngine\ExpressionCompile;
use ExpressionEngine\ExpressionEngine\ExpressionEngine;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisTag;
use ExpressionEngine\Tests\ContainerMock;
use ExpressionEngine\Tests\TestCase;

class ExpressionEngineTest extends TestCase
{
    protected ExpressionEngine $expressionEngine;

    public function setUp(): void
    {
        parent::setUp();
        $this->expressionEngine = new ExpressionEngine(new ExpressionCompile(new ContainerMock(), new LexicalAnalysisTag()));
    }

    public function dpCompile(): array
    {
        return [
            [
                '<E,0> + 1 + <V,0> >= <C,0>',
                [
                    'compile_expression_list' => [
                        '1'
                    ],
                    'compile_variable_list' => [
                        2
                    ],
                    'compile_constant_list' => [
                        3
                    ]
                ],
                true
            ]
        ];
    }

    /**
     * @dataProvider dpCompile
     * @param string $expression
     * @param array $compileContext
     * @param mixed $value
     * @return void
     * @throws LogicException
     */
    public function testCompile(string $expression, array $compileContext, mixed $value): void
    {
        $this->assertEquals($value, $this->expressionEngine->compile($expression, $compileContext));
    }

    public function dpCompileList(): array
    {
        return [
            [
                [
                    '1 + 2 + 3',
                    '<E,0> + 1 >= <V,0> + <C,0>'
                ],
                [
                    'compile_expression_list' => [
                    ],
                    'compile_variable_list' => [
                        2
                    ],
                    'compile_constant_list' => [
                        3
                    ]
                ],
                [
                    '6',
                    true
                ]
            ]
        ];
    }

    /**
     * @dataProvider dpCompileList
     * @param array $expressionList
     * @param array $compileContext
     * @param array $exceptExpressionList
     * @return void
     * @throws LogicException
     */
    public function testCompileList(array $expressionList, array $compileContext, array $exceptExpressionList): void
    {
        $this->assertEquals($exceptExpressionList, $this->expressionEngine->compileList($expressionList, $compileContext));
    }
}