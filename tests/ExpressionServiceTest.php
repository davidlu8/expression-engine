<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests;

use ExpressionEngine\ConstantEngine\ConstantEngine;
use ExpressionEngine\ExpressionEngine\ExpressionCompile;
use ExpressionEngine\ExpressionLogic;
use ExpressionEngine\ExpressionService;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisExpression;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisLoop;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisSymbol;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisTag;
use ExpressionEngine\LoopEngine\LoopEngine;
use ExpressionEngine\VariableEngine\VariableCompile;
use ExpressionEngine\VariableEngine\VariableEngine;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisEngine;
use ExpressionEngine\ExpressionEngine\ExpressionEngine;
use ExpressionEngine\VariableEngine\VariableLexical;
use ExpressionEngine\VariableEngine\VariableNode;

class ExpressionServiceTest extends TestCase
{
    protected ExpressionService $expressionService;

    public function setUp(): void
    {
        parent::setUp();
        $lexicalAnalysisTag = new LexicalAnalysisTag();
        $variableEngine = new VariableEngine(new VariableLexical(new VariableNode()), new VariableCompile($lexicalAnalysisTag));
        $constantEngine = new ConstantEngine();
        $lexicalAnalysisEngine = new LexicalAnalysisEngine(new LexicalAnalysisLoop(new ContainerMock()), new LexicalAnalysisExpression($lexicalAnalysisTag), new LexicalAnalysisSymbol($lexicalAnalysisTag, $variableEngine, $constantEngine));
        $expressionLogic = new ExpressionLogic($lexicalAnalysisEngine, $variableEngine, $constantEngine, new ExpressionEngine(new ExpressionCompile(new ContainerMock(), $lexicalAnalysisTag)));
        $loopEngine = new LoopEngine(new ContainerMock(), $expressionLogic);
        $this->expressionService = new ExpressionService($expressionLogic, $loopEngine);
    }

    public function dpCompile(): array
    {
        return [
//            [
//                "{order.start_time} <= CURRENT_DATE_TIME",
//                [
//                    'order' => [
//                        'start_time' => '2023-01-01 00:00:00',
//                        'end_time' => '2023-01-10 23:59:59',
//                    ]
//                ],
//                true
//            ],
//            [
//                '{rights.child_right.[sku_item_id={input.sku_item_id}].use_count} + ({rights.child_right.[sku_item_id={input.sku_item_id}].max_count} - {input.count})',
//                [
//                    'rights' => [
//                        'id' => 1,
//                        'sku_id' => 12345,
//                        'start_time' => '2023-01-01 00:00:00',
//                        'end_time' => '2023-01-10 23:59:59',
//                        'child_right' => [
//                            [
//                                'id' => 1,
//                                'sku_item_id' => 1,
//                                'use_count' => 0,
//                                'max_count' => 10,
//                            ],
//                            [
//                                'id' => 2,
//                                'sku_item_id' => 2,
//                                'use_count' => 10,
//                                'max_count' => 10,
//                            ]
//                        ]
//                    ],
//                    'input' => [
//                        'sku_item_id' => 2,
//                        'count' => 5,
//                    ]
//                ],
//                15
//            ],
            [
                'foreach({operation.child_rights} as {key} => {value}): {rights.child_rights.[original_id={value.original_id}].available_count} - {value.count} >= 0',
                [
                    'rights' => [
                        'child_rights' => [
                            [
                                'original_id' => 1,
                                'available_count' => 5,
                            ],
                            [
                                'original_id' => 2,
                                'available_count' => 3,
                            ],
                            [
                                'original_id' => 3,
                                'available_count' => 1,
                            ],
                        ]
                    ],
                    'operation' => [
                        'child_rights' => [
                            [
                                'original_id' => 2,
                                'count' => 3
                            ],
                            [
                                'original_id' => 3,
                                'count' => 2
                            ]
                        ]
                    ]
                ],
                false
            ]
        ];
    }

    /**
     * @dataProvider dpCompile
     * @param string $expression
     * @param array $context
     * @param mixed $value
     * @return void
     */
    public function testCompile(string $expression, array $context, mixed $value): void
    {
        $this->assertEquals($value, $this->expressionService->compile($expression, $context));
    }
}