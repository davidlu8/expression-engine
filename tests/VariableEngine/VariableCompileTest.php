<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests\VariableEngine;

use Contract\Exceptions\LogicException;
use Contract\Exceptions\ValidationException;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisTag;
use ExpressionEngine\Tests\TestCase;
use ExpressionEngine\VariableEngine\VariableCompile;

class VariableCompileTest extends TestCase
{
    protected VariableCompile $variableCompile;

    public function setUp(): void
    {
        parent::setUp();
        $this->variableCompile = new VariableCompile(new LexicalAnalysisTag());
    }

    public function dpExec(): array
    {
        return [
            [
                [
                    ['K', 'code'],
                    ['S', 'original_id', '1'],
                    ['K', 'status']
                ],
                [
                    'code' => [
                        [
                            'name' => '服务项1',
                            'original_id' => 1,
                            'status' => 1,
                        ],
                        [
                            'name' => '服务项2',
                            'original_id' => 2,
                            'status' => 2,
                        ]
                    ]
                ],
                [],
                1
            ],
            [
                [
                    ['K', 'code'],
                    ['S', 'original_id', '<V,0>'],
                    ['K', 'status']
                ],
                [
                    'code' => [
                        [
                            'name' => '服务项1',
                            'original_id' => 1,
                            'status' => 1,
                        ],
                        [
                            'name' => '服务项2',
                            'original_id' => 2,
                            'status' => 2,
                        ]
                    ]
                ],
                [
                    '2',
                ],
                2
            ]
        ];
    }

    /**
     * @dataProvider dpExec
     * @param array $variableTree
     * @param array $context
     * @param array $variableList
     * @param mixed $value
     * @return void
     * @throws LogicException
     * @throws ValidationException
     */
    public function testExec(array $variableTree, array $context, array $variableList, mixed $value): void
    {
        $this->assertEquals($value, $this->variableCompile->exec($variableTree, $context, $variableList));
    }
}