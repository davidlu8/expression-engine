<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests\VariableEngine;

use Contract\Exceptions\LogicException;
use Contract\Exceptions\ValidationException;
use ExpressionEngine\LexicalAnalysisEngine\LexicalAnalysisTag;
use ExpressionEngine\Tests\TestCase;
use ExpressionEngine\VariableEngine\VariableCompile;
use ExpressionEngine\VariableEngine\VariableEngine;
use ExpressionEngine\VariableEngine\VariableLexical;
use ExpressionEngine\VariableEngine\VariableNode;

class VariableEngineTest extends TestCase
{
    protected VariableEngine $variableEngine;

    public function setUp(): void
    {
        parent::setUp();
        $this->variableEngine = new VariableEngine(new VariableLexical(new VariableNode()), new VariableCompile(new LexicalAnalysisTag()));
    }

    public function dpCompile(): array
    {
        return [
            [
                "code.[original_id=<V,0>].use_count",
                [
                    'code' => [
                        [
                            'name' => '服务项01',
                            'original_id' => 1,
                            'max_count' => 10,
                            'use_count' => 5,
                        ],
                        [
                            'name' => '服务项02',
                            'original_id' => 2,
                            'max_count' => 3,
                            'use_count' => 2,
                        ],
                    ]
                ],
                [
                    1
                ],
                5
            ]
        ];
    }

    /**
     * @dataProvider dpCompile
     * @param string $variable
     * @param array $context
     * @param array $variableList
     * @param $value
     * @return void
     * @throws LogicException
     * @throws ValidationException
     */
    public function testCompile(string $variable, array $context, array $variableList, $expectValue): void
    {
        $this->assertEquals($expectValue, $this->variableEngine->compile($variable, $context, $variableList));
    }

    /**
     * @return array
     */
    public function dpCompileList(): array
    {
        return [
            [
                [
                    "input.original_id",
                    "code.[original_id=<V,0>].use_count",
                ],
                [
                    'code' => [
                        [
                            'name' => '服务项01',
                            'original_id' => 1,
                            'max_count' => 10,
                            'use_count' => 5,
                        ],
                        [
                            'name' => '服务项02',
                            'original_id' => 2,
                            'max_count' => 3,
                            'use_count' => 2,
                        ],
                    ],
                    'input' => [
                        'original_id' => 1
                    ]
                ],
                [
                    1,
                    5
                ]
            ]
        ];
    }

    /**
     * @dataProvider dpCompileList
     * @param array $variableList
     * @param array $context
     * @param array $variableCompileList
     * @return void
     * @throws LogicException
     * @throws ValidationException
     */
    public function testCompileList(array $variableList, array $context, array $variableCompileList): void
    {
        $this->assertEquals($variableCompileList, $this->variableEngine->compileList($variableList, $context));
    }
}