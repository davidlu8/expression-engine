<?php
declare(strict_types=1);

namespace ExpressionEngine\Tests\VariableEngine;

use Contract\Exceptions\ValidationException;
use ExpressionEngine\Tests\TestCase;
use ExpressionEngine\VariableEngine\VariableNode;
use ExpressionEngine\VariableEngine\VariableLexical;

class VariableLexicalTest extends TestCase
{
    protected VariableLexical $variableLexical;

    public function setUp(): void
    {
        parent::setUp();
        $this->variableLexical = new VariableLexical(new VariableNode());
    }


    /**
     * @return array[]
     */
    public function dpParse(): array
    {
        return [
            [
                'code.[original_id=<V,1>].status',
                [
                    ['K', 'code'],
                    ['S', 'original_id', '<V,1>'],
                    ['K', 'status']
                ]
            ]
        ];
    }

    /**
     * @dataProvider dpParse
     * @param string $variable
     * @param array $variableTree
     * @return void
     * @throws ValidationException
     */
    public function testParse(string $variable, array $variableTree): void
    {
        $this->assertEquals($variableTree, $this->variableLexical->parse($variable));
    }
}